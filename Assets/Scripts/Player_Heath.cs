using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player_Heath : MonoBehaviour {

    [SerializeField] private AudioSource DeathSoundEffect;


    void Update() {
        if (gameObject.transform.position.y < -10) {
            Die ();
        }
    }

    void Die () {
        DeathSoundEffect.Play();
        SceneManager.LoadScene ("Level_001");
    }
}
