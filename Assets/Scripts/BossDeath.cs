using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossDeath : MonoBehaviour {
    void Update() {
        if (gameObject.transform.position.y < -10)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}

