using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TelePort : MonoBehaviour
{

    public GameObject Portal;
    public GameObject Player;


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (Input.GetKeyDown("e"))
        {
            Debug.Log("E was pressed");
        }

        StartCoroutine(Teleport());
    }

    private void OnTriggerExit2D(Collider2D other)
    {

    }

    IEnumerator Teleport()
    {
        yield return new WaitForSeconds(0.5f);
        Player.transform.position = new Vector2(Portal.transform.position.x, Portal.transform.position.y);
    }
}
